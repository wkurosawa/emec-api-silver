class Api::V1::EntitiesController < ApplicationController
  def index
    @entities = Entity.all

    render json: @entities, each_serializer: EntityNoRelationshipsSerializer
  end

  def show
    @entity = Entity.find(params[:id])

    render json: @entity
  end
end