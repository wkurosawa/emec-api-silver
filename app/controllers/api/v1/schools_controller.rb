class Api::V1::SchoolsController < ApplicationController
  def index
    @schools = School.all

    render json: @schools, each_serializer: SchoolNoRelationshipsSerializer
  end

  def show
    @school = School.includes(:campuses).includes(:courses).find(params[:id])

    render json: @school, include: ['entity', 'courses', 'campuses']
  end

  def code
    school_code = params[:code].to_i

    @school = School.find_by_code(school_code)

    # TODO: Add error handling
    render json: @school, include: ['entity', 'courses', 'campuses']
  end

end