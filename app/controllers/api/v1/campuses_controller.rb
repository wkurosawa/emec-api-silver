class Api::V1::CampusesController < ApplicationController
  def index
    @campuses = Campus.all

    render json: @campuses, each_serializer: CampusNoRelationshipsSerializer
  end

  def show
    @campus = Campus.find(params[:id])

    render json: @campus
  end
end