class CourseSerializer < ActiveModel::Serializer
  attributes :code, :name, :mode, :degree, :state, :city, :enade, :cpc, :cc,
    :start_date, :periodicity, :complete_in, :minimum_credits, :seats, :coordinator, :status

  belongs_to :school
  has_many :campuses, serializer: CampusNoRelationshipsSerializer

end
