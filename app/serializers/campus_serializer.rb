class CampusSerializer < ActiveModel::Serializer
  attributes :name, :code, :pole, :address, :city, :state, :school_id, :zipcode

  belongs_to :school
  has_many :courses

end
