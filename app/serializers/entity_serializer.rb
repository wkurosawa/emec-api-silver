class EntitySerializer < ActiveModel::Serializer
  attributes :code, :name, :representative, :cnpj, :legal_nature

  has_many :schools
end
