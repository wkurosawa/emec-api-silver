class CampusNoRelationshipsSerializer < ActiveModel::Serializer
  attributes :name, :code, :pole, :address, :city, :state, :school_id, :zipcode
end
