class SchoolSerializer < ActiveModel::Serializer
  attributes :code, :name, :entity_id, :cnpj, :abbreviation, :address, :number, :complement,
    :district, :city, :zipcode, :state, :tel, :fax, :academic_organization,
    :administrative_category, :site, :email, :distance_learning, :ci, :igc

  belongs_to :entity
  has_many :campuses
  has_many :courses

end
