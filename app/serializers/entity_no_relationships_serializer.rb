class EntityNoRelationshipsSerializer < ActiveModel::Serializer
  attributes :code, :name, :representative, :cnpj, :legal_nature
end
