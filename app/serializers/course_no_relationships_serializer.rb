class CourseNoRelationshipsSerializer < ActiveModel::Serializer
  attributes :code, :name, :mode, :degree, :state, :city, :enade, :cpc, :cc,
    :start_date, :periodicity, :complete_in, :minimum_credits, :seats, :coordinator, :status
end
