class Course < ApplicationRecord
  has_and_belongs_to_many :campuses
  belongs_to :school

end
