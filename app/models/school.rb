class School < ApplicationRecord
  belongs_to :entity
  has_many :campuses
  has_many :courses

  validates :code, uniqueness: true

  def self.create_from_emec(code)
    emec_school = EmecSchool.new(code)

    entity = Entity.find_or_create_by!(code: emec_school.entity_params[:code])
    entity.update(emec_school.entity_params)

    emec_school.params[:entity_id] = entity.id
    school = School.find_or_create_by!(emec_school.params)

    entity.schools << school

    school
  end

  def update_info
    emec_school = EmecSchool.new(self.code)

    self.update(emec_school.params)
  end

  def campuses_from_emec
    emec_school = EmecSchool.new(self.code)
    emec_school.list_campuses
  end

  def courses_from_emec
    emec_school = EmecSchool.new(self.code)
    emec_school.list_courses
  end

end
