class Campus < ApplicationRecord
  belongs_to :school
  has_and_belongs_to_many :courses

  def update_info(params)
    self.address = params[:address] if params[:address].length > self.address.length
    self.zipcode = params[:zipcode]
    self.save!
  end

end
