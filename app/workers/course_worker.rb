class CourseWorker
  include Sidekiq::Worker

  def perform(school_id)
    school = School.find(school_id)

    school.courses_from_emec.each do |course|
      campuses = []

      if course.key?(:campuses)
        course[:campuses].each do |campus_id|
          campus = Campus.find(campus_id)
          puts "Course with campus: #{campus.name}"
          campuses.push(campus)
        end
      else
        campuses.push(school.campuses.first)
      end

      course.delete(:campuses)

      course = school.courses.find_or_create_by!(course)

      campuses.each do |campus|
        campus.courses << course
        campus.save!
      end
      puts "#" * 90
    end
  end
end
