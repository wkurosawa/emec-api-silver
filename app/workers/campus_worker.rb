class CampusWorker
  include Sidekiq::Worker

  def perform(school_id)
    school = School.find(school_id)

    school.campuses_from_emec.each do |campus|
      puts "#" * 90
      puts "Creating Campus: #{campus[:code]} - #{campus[:name]}"

      campus[:school_id] = school.id
      school.campuses << Campus.find_or_create_by!(campus)
    end
  end

end
