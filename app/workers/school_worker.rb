class SchoolWorker
  include Sidekiq::Worker

  def perform(code)
    School.create_from_emec(code)
  end

end