class CreateCampuses < ActiveRecord::Migration[5.0]
  def change
    create_table :campuses do |t|
      t.string :name
      t.integer :code
      t.string :pole
      t.string :address
      t.string :city
      t.string :state
      t.references :school, foreign_key: true

      t.timestamps
    end
    add_index :campuses, :code, unique: true
  end
end
