class AddCounterCachesToCampusesAndCourses < ActiveRecord::Migration[5.0]
  def change
    add_column :campuses, :courses_count, :integer, default: 0
    add_column :courses, :campuses_count, :integer, default: 0
  end
end
