class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.integer :code
      t.string :name
      t.string :mode
      t.string :degree
      t.string :state
      t.string :city
      t.float :enade
      t.float :cpc
      t.float :cc
      t.date :start_date
      t.string :periodicity
      t.float :complete_in
      t.integer :minimum_credits
      t.integer :seats
      t.string :coordinator
      t.string :status
      t.references :school

      t.timestamps
    end
    add_index :courses, :code

    create_table :campuses_courses, id: false do |t|
      t.belongs_to :campus, index: true
      t.belongs_to :course, index: true
    end

  end
end
