class CreateEntities < ActiveRecord::Migration[5.0]
  def change
    create_table :entities do |t|
      t.integer :code
      t.string :name
      t.string :representative
      t.string :cnpj
      t.string :legal_nature

      t.timestamps
    end
    add_index :entities, :code, unique: true
  end
end
