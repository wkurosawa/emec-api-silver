class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.integer :code
      t.string :name
      t.references :entity, foreign_key: true
      t.string :cnpj
      t.string :abbreviation
      t.string :address
      t.string :number
      t.string :complement
      t.string :district
      t.string :city
      t.string :zipcode
      t.string :state
      t.string :tel
      t.string :fax
      t.string :academic_organization
      t.string :administrative_category
      t.string :site
      t.string :email
      t.boolean :distance_learning
      t.float :ci
      t.float :igc

      t.timestamps
    end
    add_index :schools, :code, unique: true
  end
end
