class AddZipcodeToCampuses < ActiveRecord::Migration[5.0]
  def change
    add_column :campuses, :zipcode, :string
  end
end
