# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160927130435) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "campuses", force: :cascade do |t|
    t.string   "name"
    t.integer  "code"
    t.string   "pole"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "school_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "zipcode"
    t.integer  "courses_count", default: 0
    t.index ["code"], name: "index_campuses_on_code", unique: true, using: :btree
    t.index ["school_id"], name: "index_campuses_on_school_id", using: :btree
  end

  create_table "campuses_courses", id: false, force: :cascade do |t|
    t.integer "campus_id"
    t.integer "course_id"
    t.index ["campus_id"], name: "index_campuses_courses_on_campus_id", using: :btree
    t.index ["course_id"], name: "index_campuses_courses_on_course_id", using: :btree
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "code"
    t.string   "name"
    t.string   "mode"
    t.string   "degree"
    t.string   "state"
    t.string   "city"
    t.float    "enade"
    t.float    "cpc"
    t.float    "cc"
    t.date     "start_date"
    t.string   "periodicity"
    t.float    "complete_in"
    t.integer  "minimum_credits"
    t.integer  "seats"
    t.string   "coordinator"
    t.string   "status"
    t.integer  "school_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "campuses_count",  default: 0
    t.index ["code"], name: "index_courses_on_code", using: :btree
    t.index ["school_id"], name: "index_courses_on_school_id", using: :btree
  end

  create_table "entities", force: :cascade do |t|
    t.integer  "code"
    t.string   "name"
    t.string   "representative"
    t.string   "cnpj"
    t.string   "legal_nature"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["code"], name: "index_entities_on_code", unique: true, using: :btree
  end

  create_table "schools", force: :cascade do |t|
    t.integer  "code"
    t.string   "name"
    t.integer  "entity_id"
    t.string   "cnpj"
    t.string   "abbreviation"
    t.string   "address"
    t.string   "number"
    t.string   "complement"
    t.string   "district"
    t.string   "city"
    t.string   "zipcode"
    t.string   "state"
    t.string   "tel"
    t.string   "fax"
    t.string   "academic_organization"
    t.string   "administrative_category"
    t.string   "site"
    t.string   "email"
    t.boolean  "distance_learning"
    t.float    "ci"
    t.float    "igc"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["code"], name: "index_schools_on_code", unique: true, using: :btree
    t.index ["entity_id"], name: "index_schools_on_entity_id", using: :btree
  end

  add_foreign_key "campuses", "schools"
  add_foreign_key "schools", "entities"
end
