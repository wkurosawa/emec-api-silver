require 'emec/emec_list'
require 'emec/emec_school'
require_relative '../../app/workers/school_worker'
require_relative '../../app/workers/campus_worker'
require_relative '../../app/workers/course_worker'

namespace :school do
  desc "Create a new school fetching info from EMEC"
  task :create => :environment do
    code = ENV['CODE'].to_i

    school = School.create_from_emec(code)
  end

  desc "Check new Schools and create them"
  task :sync => :environment do
    until_page = ENV['UNTIL'].to_i if ENV['UNTIL']

    emec_list = EmecList.new
    emec_list.fetch(until_page)

    emec_codes = emec_list.schools
    system_codes = School.pluck(:code)

    puts "EMEC Schools: #{emec_codes.size}"
    puts "System Schools: #{system_codes.size}"

    new_codes = emec_codes - system_codes

    puts "New codes: #{new_codes.size}"

    new_codes.each do |code|
      if School.find_by_code(code).blank?
        puts "Creating School: #{code}"
        SchoolWorker.perform_async(code)
      end
    end
  end

  desc "Add Campuses to a specific School"
  task :add_campuses => :environment do
    code = ENV['CODE'].to_i

    school = School.find_by_code(code)
    school.campuses_from_emec.each do |campus|
      puts "Creating Campus: #{campus[:code]} - #{campus[:name]}"
      campus[:school_id] = school.id
      school.campuses << Campus.find_or_create_by!(campus)
    end
  end

  desc "Sync Campuses of all Schools"
  task :sync_campuses => :environment do
    schools = School.all

    schools.each do |school|
      unless school.campuses.exists?
        CampusWorker.perform_async(school.id)
      end
    end
  end

  desc "Sync Campuses Courses"
  task :sync_courses => :environment do
    if ENV['CODE']
      puts "🏫" * 90
      school = School.find_by_code(ENV['CODE'].to_i)
      puts "School: #{school.abbreviation} - #{school.name}"
      CourseWorker.perform_async(school.id)
    else
      schools = School.all
      schools.each do |school|
        puts "🏫" * 90
        puts "School: #{school.abbreviation} - #{school.name}"
        CourseWorker.perform_async(school.id)
      end
    end
  end

end
