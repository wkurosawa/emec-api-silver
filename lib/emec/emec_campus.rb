class EmecCampus
  require 'emec/response'
  require 'emec/sanitizer'
  require 'emec/emec_course'

  attr_accessor :params

  def initialize(school_code, code)
    @school_code = school_code
    @code = code
    @params = {}
  end

end
