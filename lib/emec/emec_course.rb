class EmecCourse
  require 'emec/response'
  require 'emec/sanitizer'

  attr_accessor :params
  attr_reader :code, :name

  def initialize(code, name)
    @code = code
    @name = name
  end

  def parse_name(text)
    cleaned = Sanitizer.clean(text)
    parenthesis = cleaned.reverse.index(')')
    cleaned.reverse[0..parenthesis - 2].reverse
  end

end
