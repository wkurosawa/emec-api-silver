class EmecList
  require 'emec/sanitizer'

  attr_accessor :schools

  def initialize
    @schools = []
  end

  def fetch(until_page = nil)
    first_page = get_schools_list(1)
    nk_page = Nokogiri::HTML(first_page)

    if until_page
      total_pages = until_page.to_i
    else
      total_pages = find_last(nk_page)
    end

    @schools = find_codes(nk_page)

    (2..total_pages).each do |i|
      page = get_schools_list(i)
      nk_page = Nokogiri::HTML(page)
      @schools.concat(find_codes(nk_page))
    end

  end

  def get_schools_list(page)
    uri = URI("http://emec.mec.gov.br/emec/nova-index/listar-consulta-avancada/list/300/page/#{page}")

    params =
    {
      "data[CONSULTA_AVANCADA][hid_template]": "listar-consulta-avancada-ies",
      "data[CONSULTA_AVANCADA][hid_order]": "ies.no_ies ASC",
      "data[CONSULTA_AVANCADA][hid_no_cidade_avancada]": "",
      "data[CONSULTA_AVANCADA][hid_no_regiao_avancada]": "",
      "data[CONSULTA_AVANCADA][hid_no_pais_avancada]": "",
      "data[CONSULTA_AVANCADA][hid_co_pais_avancada]": "",
      "data[CONSULTA_AVANCADA][rad_buscar_por]": "IES",
      "data[CONSULTA_AVANCADA][txt_no_ies]": "",
      "data[CONSULTA_AVANCADA][txt_no_curso]": "",
      "data[CONSULTA_AVANCADA][txt_no_especializacao]": "",
      "data[CONSULTA_AVANCADA][sel_co_area]": "",
      "data[CONSULTA_AVANCADA][sel_sg_uf]": "",
      "data[CONSULTA_AVANCADA][sel_st_gratuito]": "",
      "data[CONSULTA_AVANCADA][sel_no_indice_ies]": "",
      "data[CONSULTA_AVANCADA][sel_co_indice_ies]": "",
      "data[CONSULTA_AVANCADA][sel_no_indice_curso]": "",
      "data[CONSULTA_AVANCADA][sel_co_indice_curso]": "",
      "data[CONSULTA_AVANCADA][sel_co_situacao_funcionamento_ies]": "10035",
      "data[CONSULTA_AVANCADA][sel_co_situacao_funcionamento_curso]": "9",
      "data[CONSULTA_AVANCADA][sel_st_funcionamento_especializacao]": "",
      "captcha": ""
    }

    req = Net::HTTP::Post.new(uri)

    req.set_form_data(params)
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      res.body
    else
      raise "Can't reach EMEC: " + res.value
    end
  end

  def find_last(nk_page)
    select_pages = nk_page.css('#paginationControlItemdiv_listar_consulta_avancada')
    pages_options = select_pages.children.css('option')
    last_option = pages_options[pages_options.size - 1]
    last_option.attributes["title"].value.split(" ").last.to_i
  end

  def find_codes(nk_page)
    rows = nk_page.css('#tbDataGridNova').css('tr')
    codes = []
    (1..(rows.size - 2)).each do |i|
      text_code = rows[i].css('td').first.text
      code = parse_only_code(text_code)
      codes.push(code)
    end
    codes
  end

  def parse_only_code(text)
    text = Sanitizer.clean(text)
    right_parenthesis = text.index(')')
    code = text[1..right_parenthesis - 1].to_i
    code
  end

end