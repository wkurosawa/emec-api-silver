class EmecSchool
  require 'emec/response'
  require 'emec/sanitizer'
  require 'emec/emec_course'

  attr_accessor :params, :entity_params, :campuses_params

  def initialize(code)
    @code = code
    @params, @entity_params = parse_response(fetch)
    @campuses_params = []
    @courses_params = []
  end

  def fetch
    url = "http://emec.mec.gov.br/emec/consulta-ies/index/d96957f455f6405d14c6542552b0f6eb/" << Base64.encode64(@code.to_s)
    encoded_url = URI.encode(url.strip)
    uri = URI.parse(encoded_url)

    req = Net::HTTP::Get.new(uri)

    response = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end

    case response
    when Net::HTTPSuccess, Net::HTTPRedirection
      return response
    else
      raise 'Error on contacting e-MEC'
    end
  end


  def list_campuses
    html = Response.campuses(@code)
    rows = html.css('table')[2].css('tr')

    (1..(rows.size - 1)).each do |i|
      code = rows[i].css('td')[0].text.strip.to_i
      name = Sanitizer.clean(rows[i].css('td')[1].text)
      address = Sanitizer.clean(rows[i].css('td')[2].text)
      pole = Sanitizer.clean(rows[i].css('td')[3].text)
      pole = pole == '-' ? nil : pole
      city = Sanitizer.clean(rows[i].css('td')[4].text)
      state = Sanitizer.clean(rows[i].css('td')[5].text.squish)

      campus = {
        code: code,
        name: name,
        address: address,
        pole: pole,
        city: city,
        state: state
      }

      puts "Add campus #{code} - #{name}: #{address}"
      campuses_params.push(campus)
    end

    @campuses_params
  end

  def list_courses
    Response.school_courses(@code)
  end

  def parse_response(request_response)
    html_doc = Nokogiri::HTML(request_response.body)
    main_table = html_doc.css('table')

    info_table = main_table.css('table')[2]
    info_tds = info_table.css('td')

    grades_table = main_table.css('table')[3]
    grades_tds = grades_table.css('td')

    code, name, abbreviation = parse_code(info_tds[1].css('.tooltip').text.strip)

    school = {
      code: code,
      name: name,
      abbreviation: abbreviation,
      address: Sanitizer.clean(info_tds[3].text),
      number: Sanitizer.clean(info_tds[5].text),
      complement: Sanitizer.clean(info_tds[7].text),
      district: Sanitizer.clean(info_tds[11].text),
      city: Sanitizer.clean(info_tds[15].text),
      zipcode: Sanitizer.clean(info_tds[9].text),
      state: Sanitizer.clean(info_tds[17].text),
      tel: Sanitizer.clean(info_tds[19].text),
      fax: Sanitizer.clean(info_tds[21].text),
      academic_organization: Sanitizer.clean(info_tds[23].text),
      administrative_category: Sanitizer.clean(info_tds[27].text),
      site: Sanitizer.clean(info_tds[25].text),
      email: Sanitizer.clean(info_tds[29].text),
      distance_learning: info_tds[30].present? ? true : false,
      ci: grades_tds[1].text.strip.to_f,
      igc: grades_tds[4].text.strip.to_f,
    }

    entity_table = main_table.css('table')[1]
    entity_tds = entity_table.css('td')

    entity_code, entity_name = parse_entity_code(entity_tds[1].text)

    entity = {
      code: entity_code,
      name: entity_name,
      cnpj: entity_tds[3].text.strip,
      legal_nature: Sanitizer.clean(entity_tds[5].try(:text)),
      representative: Sanitizer.clean(entity_tds[7].try(:text))
    }

    return school, entity
  end

  def parse_code(text)
    text = Sanitizer.clean(text)
    right_parenthesis = text.index(')')
    code = text[1..right_parenthesis - 1].to_i
    remaining_text = text[right_parenthesis + 1..-1].lstrip
    separator = remaining_text.reverse.index(' - ')

    if separator
      abbreviation = remaining_text.reverse[0..separator - 1].reverse
      name = remaining_text.reverse[separator+3..-1].reverse
    else
      abbreviation = ''
      name = remaining_text.reverse[2..-1].reverse
    end

    return code, name, abbreviation
  end

  def parse_entity_code(text)
    text = Sanitizer.clean(text)

    right_parenthesis = text.index(')')
    code = text[1..right_parenthesis - 1].to_i
    name = text[right_parenthesis + 1..-1].lstrip

    return code, name
  end

end
