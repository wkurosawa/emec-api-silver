class Response
  BASE_URL = "http://emec.mec.gov.br/emec/"

  def self.campuses(code)
    url = ''
    url = BASE_URL + "consulta-ies/listar-endereco/d96957f455f6405d14c6542552b0f6eb/" + self.encods(code) + "/list/100"
    self.return_body(url)
  end

  def self.courses_names_from_school(code)
    url = ''
    courses_codes = []

    url = BASE_URL + "consulta-ies/listar-curso-agrupado/d96957f455f6405d14c6542552b0f6eb/" + Base64.encode64(code.to_s).strip + "/list/1000"

    codes_body = self.return_body(url)

    courses_table = codes_body.css('#listar-ies-cadastro')
    rows = courses_table.css('tr')

    (1..(rows.size - 2)).each do |i|
      detail_link = Sanitizer.clean(rows[i].css('a')[0]["href"]).strip
      slash_index = detail_link.reverse.index('/')
      code = Base64.decode64(detail_link.reverse[0..slash_index - 1].reverse).to_i

      courses_codes.push(code)
    end

    courses_codes
  end

  def self.school_courses(code)
    school = School.find_by_code code

    # Fetch Courses from School
    courses_codes = self.courses_names_from_school(code)
    # Fetch list of Course Summaries

    courses = []

    courses_codes.each do |course_code|
      course_summaries = self.course_summary(course_code, code)

      course_summaries.each do |course|
        # For each link on Course Summaries, fetch Course Detail
        course_detail = self.course_detail(course[:code])
        # puts "Merging course_detail to course #{course[:code]}"
        course.merge!(course_detail)

        course[:campuses] = []
        # Fetch course address
        course_addresses = self.course_address(course[:code])

        course_addresses.each do |course_address|
          # Match address with campuses address
          puts "%" * 90
          puts "Course address: ---------- #{course_address[:address]}"
          campuses_address = school.campuses.pluck :address

          correct_address = FuzzyMatch.new(campuses_address).find(course_address[:address])

          campus = school.campuses.find_by_address(correct_address)

          if campus
            puts "Found campus with address: #{campus.address}"
            # Merge campus id on course
            course[:campuses].push(campus.id)

            # Merge new campus info on campus
            campus.update_info(course_address)
          end
        end

        # Check if is from a 'Many address campus course'

        courses.push(course)
      end
    end

    # Push into Courses array and return
    courses
  end

  def self.course_summary(course_code, school_code)
    url = ''
    url = BASE_URL + "consulta-curso/listar-curso-desagrupado/9f1aa921d96ca1df24a34474cc171f61/" + self.encods(course_code) + "/d96957f455f6405d14c6542552b0f6eb/" + self.encods(school_code)

    summaries_body = self.return_body(url)

    summaries = []

    unless summaries_body.css('tbody').text.strip == 'Nenhum registro encontrado.'
      summaries_body.css('tr')[1..-1].each do |summary_row|
        enade = summary_row.css('td')[6].css('span').text == " - " ? 0 : summary_row.css('td')[6].css('span').text
        cpc = summary_row.css('td')[7].css('span').text == " - " ? 0 : summary_row.css('td')[7].css('span').text
        cc = summary_row.css('td')[8].css('span').text == " - " ? 0 : summary_row.css('td')[8].css('span').text

        summary = {
          code: summary_row.css('td')[0].text.to_i,
          mode: summary_row.css('td')[1].text.to_i,
          degree: summary_row.css('td')[2].text,
          name: summary_row.css('td')[3].text,
          state: summary_row.css('td')[4].text,
          city: summary_row.css('td')[5].text.gsub(" ", ''),
          enade: enade,
          cpc: cpc,
          cc: cc,
        }

        summaries.push(summary)
      end
    end

    summaries
  end

  def self.course_detail(code)
    url = ''
    url = BASE_URL + "consulta-curso/detalhe-curso-tabela/c1999930082674af6577f0c513f05a96/" + Base64.encode64(code.to_s)
    detail_body = self.return_body(url)
    table = detail_body.css('table')[1]
    rows = table.css('tr')

    start_date = rows[2].css('td')[1].text.strip.present? ? Date.parse(rows[2].css('td')[1].text.strip) : nil
    periodicity_text = rows[2].css('td')[3].text.strip
    complete_in = periodicity_text[/\(.*?\)/][1..-2].to_f
    periodicity = periodicity_text.split(' ').first
    minimum_credits = rows[3].css('td')[1].text.strip.split(' ').first.to_i
    seats = rows[3].css('td')[3].text.strip.to_i
    coordinator = rows[4].css('td')[1].text.strip.titlecase
    status = rows[5].css('td')[1].text.strip

    {
      start_date: start_date,
      periodicity: periodicity,
      minimum_credits: minimum_credits,
      complete_in: complete_in,
      seats: seats,
      coordinator: coordinator,
      status: status
    }
  end

  def self.course_address(code)
    url = ''
    url = BASE_URL + "consulta-curso/listar-endereco-curso/c1999930082674af6577f0c513f05a96/" + self.encods(code) + "/list/300"
    body = self.return_body(url)

    course_addresses = []

    body.css('tbody').css('tr').each do |row|
      course_address = {
        address: Sanitizer.clean(row.css('td')[0].text),
        zipcode: Sanitizer.clean(row.css('td')[1].text),
        city: Sanitizer.clean(row.css('td')[2].text),
        state: Sanitizer.clean(row.css('td')[3].text)
      }

      course_addresses.push(course_address)
    end

    course_addresses
  end

  private
    def self.return_body(url)
      encoded_url = URI.encode(url.strip)
      uri = URI.parse(encoded_url)

      req = Net::HTTP::Get.new(uri)

      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end

      case response
      when Net::HTTPSuccess, Net::HTTPRedirection
        return Nokogiri::HTML(response.body)
      else
        raise 'Error on contacting e-MEC'
      end
    end

    def self.encods(code)
      return Base64.encode64(code.to_s).strip
    end

end