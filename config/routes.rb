Rails.application.routes.draw do

  namespace :api, defaults: { format: :json }, path: '/', as: :api do
    scope module: :v1 do
      resources :entities, only: [:index, :show]
      resources :schools, only: [:index, :show] do
        get '/code/:code', to: 'schools#code', on: :collection, as: :code
      end
      resources :campuses, only: [:index, :show]
      resources :courses, only: [:index, :show]
    end
  end

  root 'home#home'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end
