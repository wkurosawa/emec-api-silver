#!/usr/bin/env puma
app_dir = '/var/www/emec-api/shared'

environment ENV.fetch("RAILS_ENV") { "development" }

threads 0, 16

# Only when daemonized
if @config.options[:daemon]
  puts "Starting Puma - Daemon"
  puts "APP_DIR: #{app_dir}"
  pidfile         "#{app_dir}/tmp/pids/puma.pid"
  state_path      "#{app_dir}/tmp/pids/puma.state"
  stdout_redirect "#{app_dir}/log/stdout", "#{app_dir}/log/stderr"
  bind            "unix://#{app_dir}/tmp/sockets/puma.socket"
end
