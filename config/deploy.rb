require 'mina/deploy'
require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rbenv'
require 'mina_sidekiq/tasks'

set :domain, '159.203.143.62'
set :deploy_to, '/var/www/emec-api'
set :repository, 'https://gitlab.com/wkurosawa/emec-api-silver.git'
set :branch, 'master'

set :user, 'deploy'
set :forward_agent, true
set :term_mode, nil

set :shared_dirs, fetch(:shared_dirs, []).push('log')
set :shared_files, ['config/database.yml', 'config/secrets.yml', 'config/puma.rb']

set :sidekiq_pid, "#{fetch(:shared_path)}/tmp/pids/sidekiq.pid"
set :sidekiq_log, "#{fetch(:shared_path)}/tmp/log/sidekiq.log"

task :environment do
  invoke :'rbenv:load'
end

task :setup do
  puts "-" * 90
  command "mkdir -p #{fetch(:shared_path)}/tmp/sockets"
  command "chmod g+rx,u+rwx #{fetch(:shared_path)}/tmp/sockets"

  command "mkdir -p #{fetch(:shared_path)}/tmp/pids"
  command "chmod g+rx,u+rwx #{fetch(:shared_path)}/tmp/pids"

  command "mkdir -p #{fetch(:shared_path)}/tmp/log"
  command "chmod g+rx,u+rwx #{fetch(:shared_path)}/tmp/log"

  command "mkdir -p #{fetch(:shared_path)}/config"
  command "chmod g+rx,u+rwx #{fetch(:shared_path)}/config"

  command "touch #{fetch(:shared_path)}/config/database.yml"
  command "touch #{fetch(:shared_path)}/config/secrets.yml"
  comment "Be sure to edit database.yml and secrets.yml"

end

desc "Deploys the current version to the server."
task deploy: :environment do
  # invoke :'git:ensure_pushed'
  deploy do
    invoke :'rbenv:load'
    invoke :'sidekiq:quiet'

    invoke :'git:clone'

    fetch(:shared_files, []).each do |linked_file|
      comment "Removing shared_files: #{linked_file}"
      command "rm -rf ./#{linked_file}"
    end

    invoke :'deploy:link_shared_paths'

    comment "#{fetch(:bundle_bin)}"
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      invoke :'rbenv:load'

      invoke :'sidekiq:stop'
      invoke :'sidekiq:start'

      command "chmod g+rx,u+rwx #{fetch(:current_path)}/bin/puma.sh"
      invoke :'puma:stop'
      invoke :'puma:start'
    end

  end
end

namespace :puma do
  desc "Start the application"
  task start: :environment do
    comment "Start Puma"
    command "cd #{fetch(:current_path)} && RAILS_ENV=production bin/puma.sh start"
  end

  desc "Stop the application"
  task stop: :environment do
    comment "Stop Puma"
    command "cd #{fetch(:current_path)} && RAILS_ENV=production bin/puma.sh stop"
  end

  desc "Restart the application"
  task restart: :environment do
    comment "Restart Puma"
    command "cd #{fetch(:current_path)} && RAILS_ENV=production bin/puma.sh restart"
  end
end
